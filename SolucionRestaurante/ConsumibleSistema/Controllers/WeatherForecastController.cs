﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using EntidadesSistema;
using EntidadesIntermediasSistema;
using Newtonsoft.Json;

namespace ConsumibleSistema.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;

        public WeatherForecastController(ILogger<WeatherForecastController> logger)
        {
            _logger = logger;

        }

        [HttpPost]
        public IEnumerable<WeatherForecast> Post()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
            .ToArray();
        }
                
        [HttpPost]
        [Route("prueballamada")]
        public string DevolucionJsonSP()
        {
            var rng = new Random();

            List<WeatherForecast> datos = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToList();

            WeatherForecastRequest respuesta = new WeatherForecastRequest();

            respuesta.listaWeatherForecast = datos;

            return JsonConvert.SerializeObject(respuesta);
        }

        [HttpPost]
        [Route("prueballamadaPrincipal")]
        public string DevolucionJsonC1P(string sweatherForecast)
        {
            var rng = new Random();
            WeatherForecastRequest weatherForecast = JsonConvert.DeserializeObject<WeatherForecastRequest>(sweatherForecast);

            List<WeatherForecast> datos = Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            }).ToList();

            datos.Add(weatherForecast.varWeatherForecast);

            WeatherForecastRequest respuesta = new WeatherForecastRequest();

            respuesta.listaWeatherForecast = datos;

            return JsonConvert.SerializeObject(respuesta);
        }

    }
}
