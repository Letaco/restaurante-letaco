﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using EntidadesIntermediasSistema;
using EntidadesSistema;
using Newtonsoft.Json;

namespace ConsumidorServicioLetaco
{
    public class WeatherForecastService
    {
        string cadenaLlamadaController = "";
        public WeatherForecastService() {
            cadenaLlamadaController = EstaticoSistema.conexionservicio + "/weatherforecast";
        }

        /// <summary>
        /// Llamada a servicio de consumo
        /// </summary>
        /// <param name="cadenaPrincipal">cadena de llamada con parámetros incluidos</param>
        /// <returns></returns>
        public WeatherForecastRequest ConsumoServicio(string cadenaPrincipal)
        {
            WeatherForecastRequest weatherForecastenviodatosResponse = new WeatherForecastRequest();

            //Consumiendo servicio
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@cadenaPrincipal);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream stream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(stream))
            {
                var json = reader.ReadToEnd();
                weatherForecastenviodatosResponse = JsonConvert.DeserializeObject<WeatherForecastRequest>(json);
            }
            return weatherForecastenviodatosResponse;
        }

        /// <summary>
        /// Obtiene Lista de ejemplo ListaWeatherForecast sin parámetros
        /// </summary>
        /// <returns></returns>
        public WeatherForecastRequest ListaWeatherForecast()
        {
            string cadenaPrincipal = cadenaLlamadaController + "/prueballamada";
            return ConsumoServicio(cadenaPrincipal);
        }

        /// <summary>
        /// Obtiene Lista de ejemplo ListaWeatherForecast con parámetros
        /// </summary>
        /// <param name="weatherForecastEnvioDatosRequest">Parámetro enviado al servicio</param>
        /// <returns></returns>
        public WeatherForecastRequest ListaWeatherForecast(WeatherForecastRequest weatherForecastEnvioDatosRequest)
        {            
            string cadenaPrincipal = cadenaLlamadaController + "/prueballamadaPrincipal?sweatherForecast=" + JsonConvert.SerializeObject(weatherForecastEnvioDatosRequest);
            return ConsumoServicio(cadenaPrincipal);
        }
                

        //public WeatherForecastEnvioDatos ConsumoServicio(string cadenaPrincipal, WeatherForecastEnvioDatos datosEnviados = null)
        //{

        //    WeatherForecastEnvioDatos weatherForecastenviodatosResponse = new WeatherForecastEnvioDatos();

        //    ////Consumiendo servicio
        //    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@cadenaPrincipal);
        //    request.Method = "POST";
        //    request.ContentType = "application/json; charset=utf-8";

        //    byte[] parametros = null;
        //    ////Stream postStream = null;
        //    string parametro = "";

        //    if (datosEnviados != null)
        //    {
        //        //parametro = "?sweatherForecast=" + JsonConvert.SerializeObject(datosEnviados);
        //        parametro = JsonConvert.SerializeObject(datosEnviados);
        //        parametros = Encoding.UTF8.GetBytes(parametro);
        //        request.ContentLength = parametros.Length;
        //        //postStream = request.GetRequestStream();
        //        //postStream.Write(parametros, 0, parametros.Length);
        //        using (var dataStream = request.GetRequestStream())
        //        {
        //            dataStream.Write(parametros, 0, parametros.Length);
        //        }
        //    }


        //    //Consumiendo servicio
        //    //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(@cadenaPrincipal + @parametro);
        //    //request.Method = "POST";
        //    //request.ContentType = "application/json; charset=utf-8";
        //    using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
        //    using (Stream stream = response.GetResponseStream())
        //    using (StreamReader reader = new StreamReader(stream))
        //    {
        //        var json = reader.ReadToEnd();
        //        weatherForecastenviodatosResponse = JsonConvert.DeserializeObject<WeatherForecastEnvioDatos>(json);
        //    }
        //    return weatherForecastenviodatosResponse;
        //}

    }
}
