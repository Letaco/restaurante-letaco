﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using EntidadesSistema;
using EntidadesIntermediasSistema;


namespace LetacoRestaurante
{
    public partial class Principal : Form
    {
        public Principal()
        {
            InitializeComponent();            
        }

        private void Principal_Load(object sender, EventArgs e)
        {
            WeatherForecastRequest weatherForecastEnvioDatos = EstaticosSistema.ConsumidorServicioLetaco.ListaWeatherForecast();
            
            weatherForecastEnvioDatos.varWeatherForecast = weatherForecastEnvioDatos.listaWeatherForecast.ElementAt(0);
            WeatherForecastRequest weatherForecastEnvioDatos2 = EstaticosSistema.ConsumidorServicioLetaco.ListaWeatherForecast(weatherForecastEnvioDatos);
            
            string anteponer = "";
            foreach (var item in weatherForecastEnvioDatos.listaWeatherForecast)
            {
                txtMensajes.AppendText(anteponer + string.Format("Summary: {0}, Celsius: {1}, Farenheit: {2}", item.Summary, item.TemperatureC, item.TemperatureF));
                anteponer = "\n";
            }
            anteponer = "\n\n";
            foreach (var item in weatherForecastEnvioDatos2.listaWeatherForecast)
            {
                txtMensajes.AppendText(anteponer + string.Format("Summary: {0}, Celsius: {1}, Farenheit: {2}", item.Summary, item.TemperatureC, item.TemperatureF));
                anteponer = "\n";
            }
        }
    }
}
