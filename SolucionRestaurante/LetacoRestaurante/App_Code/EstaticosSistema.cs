﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using EntidadesSistema;
using ConsumidorServicioLetaco;

namespace LetacoRestaurante
{
    public static class EstaticosSistema
    {        
        public static string Usuario = "";
        public static int idUsuario = 0;
        public static string tituloSistema = "Sistema de Administración Restaurante";
        public static WeatherForecastService ConsumidorServicioLetaco = new WeatherForecastService();
    }    
}
