﻿using System.ComponentModel;
using Xamarin.Forms;
using LetacoRestauranteMovil.ViewModels;

namespace LetacoRestauranteMovil.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}