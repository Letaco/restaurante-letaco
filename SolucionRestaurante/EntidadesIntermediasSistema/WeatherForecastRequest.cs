﻿using System;
using System.Collections.Generic;
using System.Text;
using EntidadesSistema;

namespace EntidadesIntermediasSistema
{
    [Serializable]
    public class WeatherForecastRequest
    {
        public List<WeatherForecast> listaWeatherForecast = new List<WeatherForecast>();
        public WeatherForecast varWeatherForecast = new WeatherForecast();
    }
}
